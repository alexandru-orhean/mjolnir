#!/bin/sh

ldapsearch -x -LLL -b "dc=server,dc=local" uid=$1 sshPublicKey | sed -n '/^ /{H;d};/sshPublicKey:/x;$g;s/\n *//g;s/sshPublicKey: //gp'
