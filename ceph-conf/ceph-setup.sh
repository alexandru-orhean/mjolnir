#!/bin/bash

ADMIN=0
NODE=1

sudo rpm --import 'https://download.ceph.com/keys/release.asc'
sudo yum install -y snappy leveldb gdisk gperftools-libs

sudo yum install -y python2
sudo pip install argparse
sudo pip install distribute

if [ $ADMIN -eq 1 ]
then
    sudo rpm -i /home/fedora/packages/ceph-release-1-1.el7.noarch.rpm
    sudo rpm -i --nodeps /home/fedora/packages/ceph-deploy-1.5.39-0.noarch.rpm
    sudo yum install -y /home/fedora/packages/ceph-12.2.1-0.el7.x86_64.rpm
fi

if [ $NODE -eq 1 ]
then
    sudo yum install -y ntp ntpdate ntp-doc
    sudo yum install -y firewalld
    sudo systemctl start firewalld
    sudo systemctl enable firewalld
    sudo firewall-cmd --zone=public --add-service=ceph-mon --permanent
    sudo firewall-cmd --zone=public --add-service=ceph --permanent
    sudo firewall-cmd --reload
    sudo setenforce 0
fi

if [ $ADMIN -eq 1 ]
then
    ceph-deploy --username fedora new \
        --public-network 192.168.128.0/24 greencrowbar-1
    ceph-deploy --username fedora install \
        --repo-url https://download.ceph.com/rpm-luminous/el7/ \
        greencrowbar-1 greencrowbar-2 greencrowbar-3
    ceph-deploy --username fedora mon create-initial
    ceph-deploy --username fedora admin \
        greencrowbar-1 greencrowbar-2 greencrowbar-3
    ceph-deploy --username fedora mgr create greencrowbar-1
    ceph-deploy --username fedora osd create \
        greencrowbar-1:vdb greencrowbar-2:vdb greencrowbar-3:vdb
    sudo ceph status
fi
